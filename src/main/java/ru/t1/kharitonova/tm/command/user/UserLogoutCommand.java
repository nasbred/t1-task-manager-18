package ru.t1.kharitonova.tm.command.user;

import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserLogoutCommand extends AbstractUserCommand{

    @Override
    public String getDescription() {
        return "Logout user.";
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGOUT");
        serviceLocator.getAuthService().logout();
    }

}
