package ru.t1.kharitonova.tm.api.controller;

public interface ICommandController {
    void showInfo();

    void showWelcome();

    void showAbout();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

    void showArgumentError();

    void showCommandError();
}
