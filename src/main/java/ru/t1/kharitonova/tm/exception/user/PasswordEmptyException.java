package ru.t1.kharitonova.tm.exception.user;

import ru.t1.kharitonova.tm.exception.AbstractException;

public class PasswordEmptyException extends AbstractException {

    public PasswordEmptyException() {
        super("Error! Password is empty.");
    }

}
