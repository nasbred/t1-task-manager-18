package ru.t1.kharitonova.tm.model;

import ru.t1.kharitonova.tm.api.model.IWBS;
import ru.t1.kharitonova.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Project implements IWBS {

    private String id = UUID.randomUUID().toString();
    private String name = "";
    private  String description = "";
    private Status status = Status.NOT_STARTED;
    private Date created = new Date();

    public Project() {
    }

    public Project(String name, Status status) {
        setName(name);
        setStatus(status);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

}
